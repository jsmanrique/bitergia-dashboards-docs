# Visualizations

Each chart or panel widget is called `visualization`. Bitergia provides you a full set of visualizations to play with, and build your own panels as seen before.

But, users can create or edit visualizations. Current supported visualizations are the same ones existing in [Kibana](https://www.elastic.co/guide/en/kibana/6.1/createvis.html), and those integrated by Bitergia:

* [Network Analysis](https://github.com/dlumbrer/kbn_network)
* [Search-Tables](https://github.com/dlumbrer/kbn_searchtables)
* [DotPlot](https://github.com/dlumbrer/kbn_dotplot)
* [Radar](https://github.com/dlumbrer/kbn_radar)