# Bitergia Analytics Dashboards

Bitergia Analytics dashboard is a set of panels rendering a set of visualizations (mostly charts) about project data. Panels can be linked from a navigation menu to have some information architecture and to speed up access to them.

Bitergia Analytics Dashboard is based in [Kibana](https://en.wikipedia.org/wiki/Kibana), with some extra features integrated or developed by Bitergia, like:

* [Customized Search Guard plugin](https://github.com/Bitergia/search-guard-kibana-plugin)
* Some extra visualizations like:
  * [Network Analysis](https://github.com/dlumbrer/kbn_network)
  * [Search-Tables](https://github.com/dlumbrer/kbn_searchtables)
  * [DotPlot](https://github.com/dlumbrer/kbn_dotplot)
  * [Radar](https://github.com/dlumbrer/kbn_radar)
* Navigation menu to make easier reach some panels. Upcoming releases shall add a [navigation menu editor](https://github.com/dlumbrer/kbn_dashmenu_manager).

Since, Bitergia Analytics dashboards are based in Kibana, most of [its documentation](https://www.elastic.co/guide/en/kibana/6.1/index.html) is useful for any user, taking into account the following differences:

* Bitergia Analytics dashboards don't include any Elastic X-Pack related software.
* Some features might be available or not, depending on your user profile (anonymous, admin, etc.).

While Elastic calls [dashboard](https://www.elastic.co/guide/en/kibana/6.1/dashboard.html) to each set of widgets/visualizations rendered in a canvas, we will call them *panels*.

# Index

1. [How to customize your dashboard panels](customize.md)
2. [Visualizations (AKA widgets or charts)](visualizations.md)