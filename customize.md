# Customize Bitergia Analytics dashboard panels

It's possible to change dashboard panels to fit your requirements. Current supported operations are:

* Create a new panel
* Edit existing panel
* Remove panel

To do it, you need to log in:

![Login button](./images/login-button.jpg)

Enter your username and password:

![Login screen](./images/login.jpg)

## Create a new panel

Once logged in, you can follow [Kibana documentation](https://www.elastic.co/guide/en/kibana/6.1/dashboard-getting-started.html) to build your own panel.

## Edit an existing panel

Once logged in, you can edit any panel content by clicking in the `Edit` button:

![Edit button](./images/edit.jpg)

In every widget in the panel, you'll see a //gears// icon. Click on it to get widget customization options:

![Widget options](./images/options.jpg)

Current options are:

* `Edit visualization` option to change chart parameters
* `Customzie panel` to change widget title
* `Full screen` to see the chart in full screen
* `Delete from dashboard` to remove the widget from this panel

## Remove a panel

To remove a panel, go to **Management** > **Saved objects**, and in the Dashboards tab, select those you would like to remove, and click on `Delete` button.